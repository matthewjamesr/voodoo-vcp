$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
  $(window).resize(function() {
    if ($(document).width() >= 960) {
      $('#navbarCollapse').collapse('hide')
    }
  })
})

var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    activeSection: 'Launch systems',
    localTime: '',
    timeZones: [
      { name: 'Voodoo', time: '', UTC_TEXT: 'UTC +9' },
      { name: 'Zulu', time: '', UTC_TEXT: 'UTC +0' },
      { name: 'PACAF', time: '', UTC_TEXT: 'UTC -10' }
    ],
    envUpdated: moment(),
    applications: [
      { name: "CIC Info Management", desc: 'Synchronized battle management system', resources: false, class: 'app col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4 d-inline', path: '/' },
      { name: "mIRC", desc: 'Real-time instant messaging service', resources: true, modal: '.launcher-res-mirc', class: 'app col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4 d-inline', protocol: '' },
      { name: "JMPS", desc: 'Lorem ipsum dolar asset', resources: true, modal: '.launcher-res-jmps', class: 'app col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4 d-inline', protocol: '' },
      { name: "Google Earth", desc: 'Geospational analysis services', resources: true, modal: '.launcher-res-ge', class: 'app col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 d-inline', protocol: '' },
      { name: "Outlook", desc: 'Personal & organizational mailboxes', resources: false, class: 'app col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 d-inline', protocol: 'vcp-outlook://' },
      { name: "Google Chrome", desc: 'Modern web browser', resources: false, class: 'app col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 d-inline', protocol: 'vcp-chrome://' }
    ],
    supportApplications: [
      { name: "vCP Bindings", desc: 'Modify Windows Registry enabling vCP functionality', class: 'app col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 d-inline', protocol: 'vcp-registry://' },
      { name: "CIC Setup", desc: 'Manually triger automated setup; Updates locales, settings', class: 'app col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 d-inline', protocol: 'vcp-registry://' }
    ],
    networkAppliances: [
      { name: '51 OSS/IN', type: 'drive', path: '\\\\snahgk-021dds-064v\\51OSS$', class: 'indicator bg-success', tooltip: 'Reachable' },
      { name: 'ePGL Map Data', type: 'drive', path: '\\\\snahgk-021dds-064v\map_data', class: 'indicator bg-danger', tooltip: 'Non-reachable' },
      { name: 'SF BaseDefense', type: 'drive', path: '\\\\snahgk-021dds-064v\mbdoc', class: 'indicator bg-warning', tooltip: 'Service issue' },
      { name: 'INO', type: 'printer', path: '0.0.1.1', class: 'indicator bg-warning', tooltip: 'Service issue' },
      { name: 'INW', type: 'printer', path: '0.0.2.2', class: 'indicator bg-success', tooltip: 'Reachable' },
      { name: 'Marauder', type: 'service', path: '0.0.2.2', class: 'indicator bg-success', tooltip: 'Reachable' },
      { name: 'MAT', type: 'service', path: '0.0.2.2', class: 'indicator bg-success', tooltip: 'Reachable' },
      { name: 'mIRC', type: 'service', path: '0.0.2.2', class: 'indicator bg-danger', tooltip: 'Un-reachable' }
    ]
  },
  methods: {
    updateTime: function () {
      this.timeZones[0].time = moment().format('HH:mm:ss')
      this.timeZones[1].time = moment().subtract(9,'hours').format('HH:mm:ss')
      this.timeZones[2].time = moment().subtract(19,'hours').format('HH:mm:ss')
    },
    updateActiveSection: function (title) {
      this.activeSection = title
    },
    mobileMenu: function (item, title, toggle) {
      $(item).tab('show')
      this.activeSection = title
      if (toggle) {
        $('#navbarCollapse').collapse('toggle')
      }
    },
    toggleSection: function (section) {
      $('#menu a[href="'+section+'"]').tab('show')
    }
  },
  mounted: function () {
    this.$nextTick(function () {
      window.setInterval(() => {
        this.updateTime()
      },1000);
    })
  }
})
